import java.util.Arrays;
import java.util.Random;

public class DiceGameSimulation {
    public static void main(String[] args) {
        // Simulate the first game
        int[] game1Payouts = simulateGame(1, 6, 4, 1000000);
        System.out.println("\n" + "First game payouts: "+ Arrays.stream(game1Payouts).sum());

        // Calculate and print statistics for the first game
        calculateAndPrintStatistics(game1Payouts, "Game 1");

        // Simulate the second game
        int[] game2Payouts = simulateGame(2, 6, 24, 1000000);
        System.out.println("\n" + "First game payouts: "+ Arrays.stream(game2Payouts).sum());

        // Calculate and print statistics for the second game
        calculateAndPrintStatistics(game2Payouts, "Game 2");
    }

    // Simulate a game and return an array of payouts
    private static int[] simulateGame(int numDice, int numSides, int numRolls, int numPlays) {
        int[] payouts = new int[numPlays];
        Random random = new Random();

        for (int i = 0; i < numPlays; i++) {
            int wins = 0;

            for (int j = 0; j < numRolls; j++) {
                int sum = 0;

                for (int k = 0; k < numDice; k++) {
                    // Roll each die
                    int roll = random.nextInt(numSides) + 1;
                    sum += roll;
                }

                // Check for a win condition in each roll
                if (numDice == 1 && sum == 6) {
                    wins++;
                    break; // Game 1 condition met, no need to roll further
                } else if (numDice == 2 && sum == 12) {
                    wins++;
                }
            }

            payouts[i] = wins;
        }

        return payouts;
    }

    // Calculate and print statistics for the payouts
    private static void calculateAndPrintStatistics(int[] payouts, String gameName) {
        double mean = calculateMean(payouts);
        double variance = calculateVariance(payouts, mean);
        double stdDeviation = Math.sqrt(variance);

        System.out.println("\n" + gameName + " Statistics:");
        System.out.println("Mean: " + mean);
        System.out.println("Variance: " + variance);
        System.out.println("Standard Deviation: " + stdDeviation);
    }

    // Calculate the mean of an array of values
    private static double calculateMean(int[] values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return (double) sum / values.length;
    }

    // Calculate the variance of an array of values
    private static double calculateVariance(int[] values, double mean) {
        double sumSquaredDiff = 0;
        for (int value : values) {
            double diff = value - mean;
            sumSquaredDiff += diff * diff;
        }
        return sumSquaredDiff / values.length;
    }
}
